# Tezos - Wallet Working Group - Board Meeting Minutes

## Attendees

 * Jev Bjorsell
 * Luis Gonzales
 * Samuel Bourque

## Agenda

1. Organizational Structure of WWG
1. Nominees for Membership
1. Funding/Grant Proposal

## Minutes

 * [LG] Funding and Organization structure are linked
 * [JB] Organization should be contingent on Funding
 * [JB] Charter MR could be merged when finding is approved
 * [LG] Should focus on what we can accomplish in--say--6 months
 * [SB] Proposes Scope-of-Work structure for Funding
 * [LG] Significant need for standards now; esp. HD
 * [JB] HD mnemonics standard could be a quick win
 * [SB] First SoW could include basic formation docs (e.g. glossary) and HD standard
 * [SB] Standards should mean something and be passively enforcible
 * [LG] Whitelists and Blacklists will exist--can be leveraged to arbiter
 * [LG] Large goal is Token support--this might be included in grant Proposal
 * [SB/LG] Proposal duration could be 6 months
 * [LG] Grant Proposal be drafted by Sam--should include Token support: wallet- and indexer-side as a generalized implementation
 * [JB] Could include two smaller points in Proposal: sweep keys and User-Agent definitions
 * [LG] Kukai is working on Torus integration (alternative key management scheme)--WWG should eventually cover
 * [SB] Can we choose nominees for the drafting committee?
 * [LG] We should define roles for drafting committee members
 * [JB] We might need a pool of tokens available for testing
 * [LG] Could we structure WWG as a DAO?
 * [JB] DAO is likely more appropriate for next round of funding

## Resolutions

 * Sam to produce grant Proposal
 * Scope-of-work to include: (i) HD standard; (ii) Charter and Glossary; (iii) Token support and indexing; (iv) Basic security guidelines
 * Proposal to cover 6 months
 * WWG to hold regular weekly meetings

## Follow ups

 * [JB] Confirm Default HD Derivation path
 * [LG] Status of Ledger signer
 * [SB] Review proposal next week
